# Simulation of Cardiac flows

Setup for CFD simulations of cardiac flows. 

For the LA CFD simulation:
- folder meshes_LA_idealized: three mesh levels 
- folder velocity_MV_LA_idealized: velocity profile at the mitral valve section in 7 instants during diastole. 